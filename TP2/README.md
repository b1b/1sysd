# Un jeu plus intéressant

Écrire un programme (source `game.c`) qui tire un nombre
au hasard entre 1 et 100. Et demande à l'utilisateur
une proposition. Si le joueur gagne on le félicite et
on affiche le nombre de tentatives. Sinon on lui dit
"plus grand" ou "plus petit" et on lui redemande une
proposition, jusqu'à ce qu'il trouve.
