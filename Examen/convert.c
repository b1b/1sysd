#include <stdio.h>
#include <stdlib.h>

double inches2cm(double *inches)
{
	if(*inches >= 0)
	{
		return *inches * 2.54;
	}
}

double cm2inches(double *cm)
{
	if (*cm >= 0)
	{
		return *cm / 2.54;
	}
}

int main()
{
	char convert;
	double value;
	double results;
	
	printf("Veuillez saisir 1 pour convertir les pouces en cm ou 2 pour convertir les cm en pouces : ");
	scanf("%c", &convert);
	
	printf("Veuillez saisir la valeur à convertir : ");
	scanf("%lf", &value);
	
	if (convert == '1')
	{
		results = inches2cm(&value);
		printf("%.2f pouces equivaut à %.2f centimètres.\n", value, results);
	}
	
	else if (convert == '2')
	{
		results = cm2inches(&value);
		printf ("%.2f centimètres équivaut à %.2f pouces.\n", value, results);
	}
	return 0;
}
	
