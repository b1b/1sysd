#include <stdio.h>
#include <string.h> 

int n_chiffres(char *s)
{
	int count = 0;
	for (int i = 0; i < strlen(s); i++)
	{
		if (s[i] >= '0' && s[i] <= '9')
		{
			count++; //ici le count est un compteur, il va s'incrémenter de 1 si s rencontr un chiffre
		}
	}
	return count;
}

int main(int argc, char *argv[])
{	
	if(argc < 2) 
	{
        printf("Veuillez fournir une chaîne de caractères en argument.\n");
        return 1;
    	}
	
	printf("La chaîne contient %d chiffres.\n", n_chiffres(argv[1]));
	return 0;
}
