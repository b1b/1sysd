#include <stdio.h>

int str_to_up(char c) 
{
    if(c >= 'a' && c <= 'z') 
    {
        return c - 'a' + 'A';
    }
    return c;
}

int nocase_equal(char *s1, char *s2) 
{
    int i = 0;
    while(s1[i] != '\0' || s2[i] != '\0') 
    {
        if(str_to_up(s1[i]) != str_to_up(s2[i])) 
        {
            return 0;
        }
        i++;
    }
    return 1;
}

int main() 
{
    char s1[] = "Mr Anderson";
    char s2[] = "Got my package ?";
    if(nocase_equal(s1, s2)) 
    {
        printf("Les deux chaînes sont identiques (sans tenir compte de la casse).\n");
    } else 
    {
        printf("Les deux chaînes sont différentes.\n");
    }
    return 0;
}
